clc; clear all;

B = load('briaunos.txt')
m = length(B(1, :));        % briaunu skaicius
n = max(max(B));            % virsuniu skaicius
d(1:n) = 0;

% Laipsniu skaiciavimas
for i = 1:m
    k = B(1, i);
    d(k) = d(k) + 1;
end

d

lst(1:n+1) = 0;     % virsuniu adresu masyvas lst
for i = 1:n
    lst(i+1) = lst(i) + d(i);
end;

L(1:m) = 0;         % masyvas L
fst = lst + 1;      % indeksai, nurodantys kur masyve L talpinti gretima virsune

for j = 1:m
    k = B(1, j);
    L(fst(k)) = B(2, j);
    fst(k) = fst(k) + 1;
end;

L
lst
  