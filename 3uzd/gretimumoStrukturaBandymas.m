clc; clear all;

B = [1 1 1 2 2 3 4;
     2 4 5 3 6 4 5];
m = length(B(1, :));    % briaunu kiekis
n = max(max(B));        % virsuniu kiekis

GR = gretimumoStruktura(B, m, n, true);

GR                      % spausdinama gretimumo struktura

for i = 1:n
    disp(GR{i});
end;