% Procedura grafdaz1 da�o grafo vir�unes, remdamasi euristika �pirma spalva, o po to vir�ene�.
% Formalus parametrai:
% n � grafo vir�uniu skaicius,
% m � grafo briaunu (lanku) skaicius,
% b � grafo briaunu matrica ,
% p � spalvu skaicius,
% d � vir�uniu spalvu masyvas;
% jei d(i) = k, tai rei�kia, kad i-oji vir�une da�oma k-aja spalva.
function [t, d] = grfdaz1 (n, m, b, p, d)
    [L, lst] = BLlst (n, m, b);
    % Pradiniu reik�miu suteikimas darbo masyvams ir kintamiesiems
    for i = 1:n
        v(i) = i;                      % Masyve v i� eiles sura�omi vir�uniu numeriai
        s(i) = lst(i+1)-lst(i);        % s (i) � i-tosios vir�unes laipsnis
        d(i) = 0;
    end
    % Virsunes masyve v i�rikiuojame ju laipsniu ma�ejimo tvarka
    
    pakeista = 0;
    
    for k = 1:n-1
        for i = 1:n-k
            if s(i) < s(i+1)            % Keiciame vietomis s(i) su s(i+1) ir v(i) su v(i+1)
                z = s (i); s(i) = s(i+1); s(i+1) = z;
                z = v (i); v(i) = v(i+1); v(i+1) = z;
            end
            p = 0;                      % p � spalvu skaicius
            sv = 0;                     % sv - nuda�ytu vir�uniu skaicius
            if(pakeista < n)
                while sv < n
                    p = p + 1;
                    for i = 1:n
                        u = v(i);
                        if d (u) == 0       % Vir�une u � nenuda�yta.
                                            % Ar ju galima da�yti p-aja spalva?
                                            % Ar vir�uniu, gretimu vir�unei u, tarpe yra vir�une,nuda�yta p-aja spalva?
                            j = lst(u)+1; 
                            t = false;
                            % Jei t = false, tai vir�une u galima da�yti p-aja spalva
                            while (j <= lst(u+1)) && ~t
                                x = L(j);
                                if d(x) == p
                                    t = true;
                                else
                                    j = j+1;
                                end
                            end
                            if ~t
                                d(u) = p;
                                sv = sv + 1;
                                pakeista = pakeista + 1;
                            end
                        end
                    end
                end
            else
                t = true;
            end
        end
    end
end