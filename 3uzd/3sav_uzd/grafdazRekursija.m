function [t, sp] = grafdazRekursija(n, m, b, k)
    [L, lst] = BLlst (n, m, b);
    sp(1, n) = 0;
    [t, sp] = gilyn(L, lst, 1, n, k, sp);
end

function [t, sp] = gilyn(L, lst, vv, n, k, sp)
    if vv == n + 1
        t = true;
        return;
    end;
    for c = 1:k
        Yra = tikrink(L, lst, vv, sp, c);
        if Yra
            sp(vv) = c;
            [t, sp] = gilyn(L, lst, vv+1, n, k, sp);
            if t == true
                return;
            end
            sp(vv) = 0;
        end
        t = false;
    end
end
function Yra = tikrink(L, lst, vv, sp, c)
    Yra = true;
    for i = lst(vv)+1:lst(vv+1)
        if sp(L(i)) == c
            Yra = false;
            break;
        end
    end
end