clc; clear all; close all;
B = load('Moduliai.txt');
spKiekis = input('Iveskite egzaminavimo laiku skaiciu: ');
vspalvos = 'rgbcmyk';
m = length(B(1, :));
n = max(max(B));
V = 1:n;
U = [];
d = zeros(1, n);

for BrNr = 1:m
    U{BrNr} = B(:, BrNr);
end
virsuniu = numel(V);
briaunu = numel(U);

[t, d] = grfdaz1(virsuniu, briaunu, B, spKiekis, d);

figure(1);
Vkor = plotGraphVU1(V, U, 0, 0, [], 0, 10, 1, 'b');
if t == false
    fprintf('Sprendinys neegzistuoja\n');
else
    fprintf('Viso egzam.laiku skaicius: %3d\n', spKiekis);
    fprintf('odulis Egzm.Laikas:\n');
    for i = 1:virsuniu
        fprintf('%5d %6d\n', i, d(i));
        Vkori = Vkor(i, :);
        figure(1);
        spalva(Vkori, vspalvos(d(i)), i);
    end
end

fprintf('Rekursines funkcijos ivykdymo laikas:\n');
tic
    [t, d] = grafdazRekursija(virsuniu, briaunu, B, spKiekis);
toc

fprintf('Pirma spalva tada virsune funkcijos ivykdymo laikas:\n');
tic
    [t, d] = grfdaz1(virsuniu, briaunu, B, spKiekis, d);
toc