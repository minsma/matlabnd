function [L, lst] = BLlst(n, m, b)
% BLlst perveda neorentuotojo grafo briaunu matrica
% i tioesioginiu nuorodu masyvus L ir lst.
% Virsuniu laipsniu apskaiciavimas.
    d(1:n) = 0;
    for i = 1:2
        for j = 1:m
            k = b(i, j);
            d(k) = d(k) + 1;
        end
    end
    lst(1) = 0;         % masyvo lst formavimas
    for i = 1:n
        lst(i+1) = lst(i) + d(i);
    end
    % indeksai, nurodantys kur masyve L talpinti gretima virsune
    fst = lst + 1;
    L(1:2*m) = 0;       % masyvo L formavimas
    for j = 1:m
        k = b(1, j);
        L(fst(k)) = b(2, j);
        fst(k) = fst(k) + 1;
        k = b(2, j);
        L(fst(k)) = b(1, j);
        fst(k) = fst(k) + 1;
    end
end