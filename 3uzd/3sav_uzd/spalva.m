function spalva(Vkor, sp, nr)
    r = 0.08;                       % virsunes spindulys
    x = Vkor(1, 1); y = Vkor(1, 2); % V(i) virsunes koordinates
    rectangle('Position', [x-r, y-r, 2*r, 2*r], 'Curvature', [1,1], 'FaceColor', sp);
    % uzrasomas virsunes numeris:
    if abs(nr) < 10
        shiftx = 0.2*r;
    else
        shiftx = 0.6*r;
    end
    str = sprintf('%d', abs(nr));
    text(x-shiftx, y, str);
end