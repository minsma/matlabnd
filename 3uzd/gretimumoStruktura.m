function GR = gretimumoStruktura(B, m, n, orientuotas)
    GR{n} = [];
    d(1:n) = 0;             % laipsniai
    
    if orientuotas == false             % jeigu ne orgrafas
       for j = 1:m
            k = B(1, j);
            d(k) = d(k) + 1;
            GR{k} = [GR{k}, B(2, j)];
            k = B(2, j);
            d(k) = d(k) + 1;
            GR{k} = [GR{k}, B(1, j)];        
       end
    else                                % jeigu orgrafas
        for j = 1:m
            k = B(1, j);
            d(k) = d(k) + 1;
            GR{k} = [GR{k}, B(2, j)];
        end
    end
end