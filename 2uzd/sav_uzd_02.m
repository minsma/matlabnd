function Convertion
    clc; clear all;
    taskNumber = 9;
    binCode = DecToBin(taskNumber - 1);
    number = '01111111';
    grayCode = DecToGray(number, binCode)
end

function B = DecToBin(sk)
    i = 1;

    while sk > 0
        A(i) = mod(sk,2);
        sk = floor(sk/2);
        i = i + 1;
    end;
    
    B = A(end:-1:1);
end

function numberCopy = DecToGray(number, binCode)
    x4 = binCode(length(binCode));
    x3 = binCode(length(binCode) - 1);
    x2 = binCode(length(binCode) - 2);
    x1 = binCode(length(binCode) - 3);
    
    numberCopy = number;
        
    for i = 2:1:length(number)
        if i+1 <= length(number)
            if number(i) == '0' && number(i+1) == '0'
                numberCopy(i+1) = int2str(x1);
            elseif number(i) == '0' && number(i+1) == '1'
                numberCopy(i+1) = int2str(x2);
            elseif number(i) == '1' && number(i+1) == '0'
                numberCopy(i+1) = int2str(x3);
            elseif number(i) == '1' && number(i+1) == '1'
                numberCopy(i+1) = int2str(x4);
            end
        end
    end
end
