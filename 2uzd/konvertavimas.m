function Konvertavimas
clc; clear all;
sk = 17; % duotas desimtainis skaicius
B1 = DecToBin(sk);
disp([int2str(sk) ' ---> ' int2str(B1(1, :))]);
B2 = DecToBin(25);
disp(['25 --> ' int2str(B2(1,:))]);
end

function B = DecToBin(sk)
i = 1;
% A - kaupiamos sk. sk dalybos is 2 liekanos
while sk > 0
    A(i) = mod(sk,2);
    sk = floor(sk/2);
    i = i + 1;
end;
% formuojamas dvejetainis sk. B is gautu 1 ir 0
B = A(end:-1:1);
end