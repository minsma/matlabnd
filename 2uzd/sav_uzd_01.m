function Convertion
    clc; clear all;
    number = 10101010;
    A = BinToDec(number)
end

function A = BinToDec(number)
    A = 0; % desimtainiui skaiciui saugoti skirtas kintamasis
    
    numberInChars = int2str(number); % skaicius paverciamas char masyvu
    lengthOfNumber = length(numberInChars) - 1; % skaiciaus ilgis
    
    for i = 1:1:lengthOfNumber
        if numberInChars(i) == '1' % jeigu vienetas
           A = A + power(2, lengthOfNumber); % 2 pakeltas laipsniu pridedamas prie sumos
        end
        
        lengthOfNumber = lengthOfNumber - 1;
    end
end
