function b = atvSk(a)

b = 0;

while a > 0
    b = mod(a, 10) + b * 10;
    a = floor(a/10);
end;

end