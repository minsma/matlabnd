clc; close all; clear all;
U = false(100, 100); % aibe 100x100

A = U;
A(:, 1:50) = true;
B = U;
B(30:50, :) = true;
C = U;
C(50:75, :) = true;

figure(1); imshow(B | (A & C));

D = U;
D(1:20, :) = true;
E = U;
E(:, 50:100) = true;

figure(2); imshow((D & E));

F = U;
F(1:50, :) = true;
G = U;
G(50:100, :) = true;

figure(3); imshow(~(A & F) & ~(E & G));