clc; close all; clear all;
n = 100;
U = ones(n, n);

c = 1;

while n >= 1
    for i = 1:1:n
        U(c, i) = 0;
    end;
    n = n - 1;
    c = c + 1;
end;

figure; imshow(U);