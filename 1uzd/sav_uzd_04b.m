clc; close all; clear all;
n = 100;
U = ones(n, n);

begin = 1;
ending = n;

counter = 1;

while counter <= n
    for i = begin:1:ending
        U(counter, i) = 0;
    end
    begin = begin + 1;
    ending = ending - 1;
    counter = counter + 1;
end

figure; imshow(U);